from twilio.twiml.messaging_response import MessagingResponse
from flask import Flask
from flask import request
app = Flask(__name__)


""" The first thing we need to do in our chatbot is obtain the message entered by the user. 
    This message comes in the payload of the POST request with a key of ’Body’.
    We can access it through Flask’s request object"""

@app.route('/bot', methods=['POST'])
def bot():

    incoming_message = request.values.get('Body').lower()

    response = MessagingResponse()
    message = response.message()
    message.body("answer ")
    return 'Hello World!'


if __name__ == '__main__':
    app.run()
